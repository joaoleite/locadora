<!doctype html>
<?php

session_start();

?>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../imagens/favicon.ico">

    <title>Sistema de Aluguel de Carros</title>

    <!-- Bootstrap core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./css/signin.css" rel="stylesheet">
  <script type="text/javascript" src="main.js" charset="UTF-8"></script><link rel="stylesheet" crossorigin="anonymous" 
  href="main.css"/></head>
        
  
  <body>

    <div class="container">

        <form class="form-signin" method="POST" action="#">
          <h2  class="form-signin-heading">LOGIN DO SISTEMA</h2></br>
        <label for="inputEmail" class="sr-only">CPF</label>
        <input type="text" id="cpf" class="form-control" placeholder="Numero do CPF" required autofocus>
        </br> <label for="inputPassword" class="sr-only">senha</label>
        <input type="password" id="senha" class="form-control" placeholder="Senha" required>
        </br></br>
        
        <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
      </form>

    </div> <!-- /container -->
   
  </body>
  
</html>
