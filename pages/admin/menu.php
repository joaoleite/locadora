<!doctype html>
<html lang=''>
    <head>
        <meta charset='utf-8'>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="./css/styles.css">
        <script src="./js/script.js" type="text/javascript"></script>
        <script src="./js/script.js"></script>
        <title>CSS MenuMaker</title>
    </head>
    <body>

        <div id='cssmenu'>
            <ul>
                <li><a href='#'><span>Home</span></a></li>
                <li class='active has-sub'><a href='#'><span>Funcionarios</span></a>
                    <ul>
                        <li class='has-sub'><a href='#'><span>Cadastrar</span></a>
                        <li class='has-sub'><a href='#'><span>Consultar</span></a>
                        <li class='has-sub'><a href='#'><span>Alterar Senha</span></a>
                        <li class='has-sub'><a href='#'><span>Alterar Dados</span></a>

                    </ul>
                </li>
                <li class='active has-sub'><a href='#'><span>Clientes</span></a>
                    <ul>
                        <li class='has-sub'><a href='#'><span>Cadastrar</span></a>
                        <li class='has-sub'><a href='#'><span>Consultar</span></a>
                        <li class='has-sub'><a href='#'><span>Alterar Dados</span></a>
                    </ul>
                </li>
                <li class='active has-sub'><a href='#'><span>Carros</span></a>
                    <ul>
                        <li class='has-sub'><a href='#'><span>Cadastrar</span></a>
                        <li class='has-sub'><a href='#'><span>Consultar</span></a>
                        <li class='has-sub'><a href='#'><span>Alterar</span></a>
                            
                    </ul>
                </li>
                <li class='active has-sub'><a href='#'><span>Aluguel</span></a>
                    <ul>
                        <li class='has-sub'><a href='login.php'><span>Cadastrar</span></a>
                        <li class='has-sub'><a href='#'><span>Consultar</span></a>
                        <li class='has-sub'><a href='#'><span>Concluir Aluguel</span></a>
                    </ul>
                </li>
            </ul>
        </div>

    </body>
</html>
