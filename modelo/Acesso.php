<?php

class Acesso{
    private $idAcesso;
    private $nivel;
    
    function __construct() {      
    }
    
    function getIdAcesso() {
        return $this->idAcesso;
    }

    function getNivel() {
        return $this->nivel;
    }

    function setIdAcesso($idAcesso) {
        $this->idAcesso = $idAcesso;
    }

    function setNivel($nivel) {
        $this->nivel = $nivel;
    }



}
