<?php

class Sinistro{
    private $idSinistro;
    private $descricao;
    private $valor_a_pagar;
    
    function __construct() {     
    }
    
    function getIdSinistro() {
        return $this->idSinistro;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getValor_a_pagar() {
        return $this->valor_a_pagar;
    }

    function setIdSinistro($idSinistro) {
        $this->idSinistro = $idSinistro;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setValor_a_pagar($valor_a_pagar) {
        $this->valor_a_pagar = $valor_a_pagar;
    }



}

