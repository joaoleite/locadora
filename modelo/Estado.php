<?php

class Estado{
    private $idEstado;
    private $nome;
    private $sigla;
    
    function __construct() {    
    }

    function getIdEstado() {
        return $this->idEstado;
    }

    function getNome() {
        return $this->nome;
    }

    function getSigla() {
        return $this->sigla;
    }

    function setIdEstado($idEstado) {
        $this->idEstado = $idEstado;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setSigla($sigla) {
        $this->sigla = $sigla;
    }


}
?>
