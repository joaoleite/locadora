<?php

class Carro{
    private $idCarro;
    private $marca;
    private $modelo;
    private $cor;
    private $ano;
    private $kilometragem;
    private $valorDiaria;
    private $acessoriosDesc;
    private $situacao;
    
    function __construct() {    
    }
    
    function getIdCarro() {
        return $this->idCarro;
    }

    function getMarca() {
        return $this->marca;
    }

    function getModelo() {
        return $this->modelo;
    }

    function getCor() {
        return $this->cor;
    }

    function getAno() {
        return $this->ano;
    }

    function getKilometragem() {
        return $this->kilometragem;
    }

    function getValorDiaria() {
        return $this->valorDiaria;
    }

    function getAcessoriosDesc() {
        return $this->acessoriosDesc;
    }

    function getSituacao() {
        return $this->situacao;
    }

    function setIdCarro($idCarro) {
        $this->idCarro = $idCarro;
    }

    function setMarca($marca) {
        $this->marca = $marca;
    }

    function setModelo($modelo) {
        $this->modelo = $modelo;
    }

    function setCor($cor) {
        $this->cor = $cor;
    }

    function setAno($ano) {
        $this->ano = $ano;
    }

    function setKilometragem($kilometragem) {
        $this->kilometragem = $kilometragem;
    }

    function setValorDiaria($valorDiaria) {
        $this->valorDiaria = $valorDiaria;
    }

    function setAcessoriosDesc($acessoriosDesc) {
        $this->acessoriosDesc = $acessoriosDesc;
    }

    function setSituacao($situacao) {
        $this->situacao = $situacao;
    }



}

