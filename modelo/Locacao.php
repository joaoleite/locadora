<?php

class Locacao{
    private $idLocacao;
    private $data;
    private $data_para_entrega;
    private $valorTotal;
    
    
    function __construct() {       
    }
    
    function getIdLocacao() {
        return $this->idLocacao;
    }

    function getData() {
        return $this->data;
    }

    function getData_para_entrega() {
        return $this->data_para_entrega;
    }

    function getValorTotal() {
        return $this->valorTotal;
    }

    function setIdLocacao($idLocacao) {
        $this->idLocacao = $idLocacao;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setData_para_entrega($data_para_entrega) {
        $this->data_para_entrega = $data_para_entrega;
    }

    function setValorTotal($valorTotal) {
        $this->valorTotal = $valorTotal;
    }



}

