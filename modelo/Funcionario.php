<?php

class Funcionario{
    private $idFuncionario;
    private $login;
    private $senha;
    
    function __construct() {   
    }
    
    function getIdFuncionario() {
        return $this->idFuncionario;
    }

    function getLogin() {
        return $this->login;
    }

    function getSenha() {
        return $this->senha;
    }

    function setIdFuncionario($idFuncionario) {
        $this->idFuncionario = $idFuncionario;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }



}

