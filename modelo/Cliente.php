<?php

class Cliente{
    private $idCliente;
    private $cnh;
    private $validadeCnh;
    
    function __construct() {     
    }
    
    function getIdCliente() {
        return $this->idCliente;
    }

    function getCnh() {
        return $this->cnh;
    }

    function getValidadeCnh() {
        return $this->validadeCnh;
    }

    function setIdCliente($idCliente) {
        $this->idCliente = $idCliente;
    }

    function setCnh($cnh) {
        $this->cnh = $cnh;
    }

    function setValidadeCnh($validadeCnh) {
        $this->validadeCnh = $validadeCnh;
    }



}
